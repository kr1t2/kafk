package org.example.krit.kafk.tasks;

import org.bukkit.scheduler.BukkitRunnable;
import org.example.krit.kafk.KAFK;
import org.example.krit.kafk.utils.SetPlayerAfk;

import java.util.Map;
import java.util.UUID;

public class CheckAfkPlayers extends BukkitRunnable {
    private static final CheckAfkPlayers instance = new CheckAfkPlayers();

    @Override
    public void run() {

        // Это выполняется каждые 100 тиков
        // Проверка игроков на афк

        for (Map.Entry<UUID, Long> entry : KAFK.afkPlayers.entrySet()) {
            UUID playerUUID = entry.getKey();
            Long timeStartedAFK = entry.getValue();

            Long totalAfkTime = System.currentTimeMillis() - timeStartedAFK;

            SetPlayerAfk.setPlayerAFK(playerUUID, totalAfkTime);
        }
    }

    public static CheckAfkPlayers getInstance() {
        return instance;
    }
}
