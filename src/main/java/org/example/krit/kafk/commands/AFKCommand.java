package org.example.krit.kafk.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.example.krit.kafk.KAFK;
import org.example.krit.kafk.utils.SetPlayerAfk;
import org.example.krit.kafk.utils.SetPlayerNoAfk;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class AFKCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {

        if (sender instanceof Player p) {
            UUID playerUUID = p.getUniqueId();
            if (KAFK.infinityAFKPlayers.containsKey(playerUUID)) {
                KAFK.infinityAFKPlayers.remove(playerUUID);

                SetPlayerNoAfk.setPlayerNoAfk(playerUUID);

            } else {
                KAFK.infinityAFKPlayers.put(playerUUID, System.currentTimeMillis());
                SetPlayerAfk.setPlayerAFK(playerUUID, System.currentTimeMillis());
            }
        }

        return true;
    }
}
