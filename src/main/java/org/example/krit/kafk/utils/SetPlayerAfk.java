package org.example.krit.kafk.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.example.krit.kafk.KAFK;

import java.util.UUID;

public class SetPlayerAfk {

    // Сделать игрока афк

    public static void setPlayerAFK(UUID playerUUID, Long totalAfkTime) {

        if (totalAfkTime / 1000 > Integer.parseInt(KAFK.getPlugin().getConfig().getString("time-to-afk")) && !KAFK.nowAFkPlayers.containsKey(playerUUID)) {
            Player afkPlayer = Bukkit.getPlayer(playerUUID);

            KAFK.getPlugin().getServer().getGlobalRegionScheduler().run(KAFK.getPlugin(), scheduledTask ->
                    KAFK.getPlugin().getServer().dispatchCommand(KAFK.getPlugin().getServer().getConsoleSender(),KAFK.getPlugin().getConfig().getString("command-to-add-prefix").replace("<player>", Bukkit.getPlayer(playerUUID).getName()))
            );

            KAFK.nowAFkPlayers.put(playerUUID, System.currentTimeMillis());
            KAFK.afkPlayers.replace(playerUUID, System.currentTimeMillis());

            if (KAFK.infinityAFKPlayers.containsKey(playerUUID)) {
                afkPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', KAFK.getPlugin().getConfig().getString("afk-infinity-message")));
            } else {
                afkPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', KAFK.getPlugin().getConfig().getString("afk-message")));
            }

        }
    }
}
