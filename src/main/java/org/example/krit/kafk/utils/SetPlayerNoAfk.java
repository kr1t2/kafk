package org.example.krit.kafk.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.example.krit.kafk.KAFK;

import java.util.UUID;

public class SetPlayerNoAfk {

    // Сделать игрока не афк

    public static void setPlayerNoAfk(UUID playerUUID) {
        if (!KAFK.nowAFkPlayers.containsKey(playerUUID)) {
            return;
        }
        Long totalAfkTime = System.currentTimeMillis() - KAFK.nowAFkPlayers.get(playerUUID);
        if (totalAfkTime <= 1000) {
            return;
        }
        Player afkPlayer = Bukkit.getPlayer(playerUUID);

        KAFK.getPlugin().getServer().getGlobalRegionScheduler().run(KAFK.getPlugin(), scheduledTask ->
                KAFK.getPlugin().getServer().dispatchCommand(KAFK.getPlugin().getServer().getConsoleSender(), KAFK.getPlugin().getConfig().getString("command-to-disable-prefix").replace("<player>", afkPlayer.getName()))
        );
        
        afkPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', KAFK.getPlugin().getConfig().getString("afk-stop-message").replaceAll("<time>", ConvertMillisToDate.millisecToTime(totalAfkTime))));
        afkPlayer.decrementStatistic(Statistic.TOTAL_WORLD_TIME, (int) (totalAfkTime / 50));

        KAFK.nowAFkPlayers.remove(playerUUID);
        KAFK.afkPlayers.replace(playerUUID, System.currentTimeMillis());
    }

}
