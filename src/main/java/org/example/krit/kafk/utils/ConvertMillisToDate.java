package org.example.krit.kafk.utils;

public class ConvertMillisToDate {

    public static String millisecToTime(long millisec) {
        long sec = millisec / 1000;
        long second = sec % 60;
        long minute = sec / 60;
        if (minute >= 60) {
            long hour = minute / 60;
            minute %= 60;
            return hour + "ч. " + (minute < 10 ? "0" + minute : minute) + "мин. " + (second < 10 ? "0" + second : second + "сек.");
        }
        return minute + "мин. " + (second < 10 ? "0" + second : second) + "сек.";
    }

}
