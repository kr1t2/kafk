package org.example.krit.kafk.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.example.krit.kafk.KAFK;
import org.example.krit.kafk.utils.SetPlayerNoAfk;

import java.util.UUID;

public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        
        UUID playerUUID = e.getPlayer().getUniqueId();
        if (KAFK.infinityAFKPlayers.containsKey(playerUUID)) {
            return;
        }
        if (KAFK.nowAFkPlayers.containsKey(playerUUID)) {
            SetPlayerNoAfk.setPlayerNoAfk(playerUUID);
            return;
        }
        KAFK.afkPlayers.replace(playerUUID, System.currentTimeMillis());

    }
}
