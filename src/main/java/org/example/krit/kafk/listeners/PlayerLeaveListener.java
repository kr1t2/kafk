package org.example.krit.kafk.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.example.krit.kafk.KAFK;
import org.example.krit.kafk.utils.SetPlayerNoAfk;

import java.util.UUID;

public class PlayerLeaveListener implements Listener {

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent e) {
        UUID playerUUID = e.getPlayer().getUniqueId();
        if (KAFK.nowAFkPlayers.containsKey(playerUUID) || KAFK.infinityAFKPlayers.containsKey(playerUUID)) {
            SetPlayerNoAfk.setPlayerNoAfk(playerUUID);
        }
        KAFK.afkPlayers.remove(playerUUID);
        KAFK.nowAFkPlayers.remove(playerUUID);
        KAFK.infinityAFKPlayers.remove(playerUUID);
    }
}
