package org.example.krit.kafk;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.example.krit.kafk.commands.AFKCommand;
import org.example.krit.kafk.listeners.PlayerJoinListener;
import org.example.krit.kafk.listeners.PlayerLeaveListener;
import org.example.krit.kafk.listeners.PlayerMoveListener;
import org.example.krit.kafk.tasks.CheckAfkPlayers;
import org.example.krit.kafk.tasks.Scheduler;

import java.util.HashMap;
import java.util.UUID;

public final class KAFK extends JavaPlugin {

    public static HashMap<UUID, Long> afkPlayers = new HashMap<>();
    public static HashMap<UUID, Long> nowAFkPlayers = new HashMap<>();

    public static HashMap<UUID, Long> infinityAFKPlayers = new HashMap<>();

    private static KAFK plugin;

    @Override
    public void onEnable() {

        PluginManager pm = Bukkit.getServer().getPluginManager();
        if (pm.getPlugin("TAB") == null ) {
            Bukkit.getLogger().info("TAB плагин должен быть установлен!");
            Bukkit.getLogger().info("Загрузите его здесь: https://www.spigotmc.org/resources/tab-1-5-1-20-5.57806/");

            return;
        }

        plugin = this;

        // Сохранение конфига
        getConfig().options().copyDefaults();
        saveDefaultConfig();

        //События
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerLeaveListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerMoveListener(), this);

        //Команды
        getCommand("afk").setExecutor(new AFKCommand());

        //Задачки
        Scheduler.runTimer(CheckAfkPlayers.getInstance(), 0L, 100L);
    }

    public static KAFK getPlugin() {
        return plugin;
    }
}
